
package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature {
	public Celsius(float t) {
		super(t);
		}
	public String toString()
	{
		return "" + this.getValue();
		}
	
	public Temperature toCelsius() {
		return this;
	}

	public Temperature toFahrenheit() {
		return new Celsius((float)(getValue()*(9.0/5)+32));
	}
  }

