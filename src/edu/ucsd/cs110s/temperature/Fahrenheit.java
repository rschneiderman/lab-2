/**
 * 
 */
package edu.ucsd.cs110s.temperature;
 
/**
 * @author cs110sat
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t){
		
		super(t);
		}
	public String toString() {
		return "" + this.getValue();
		}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
	@Override
	public Temperature toCelsius() {
		return new Fahrenheit((float)((getValue()-32)*(5.0/9.0)));
	}
}
